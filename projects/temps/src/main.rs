use std::io;

fn main() {
    println!("Convert temperatures between Fahrenheit and Celcius!");

    loop {
        println!("Please input temperature.");
        let mut temp = String::new();

        io::stdin()
            .read_line(&mut temp)
            .expect("Failed to read line");

        let temp: f64 = match temp.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("Convert to Fahrenheit(f) or Celsius(c)?");
        let mut convert_to_unit = String::new();

        io::stdin()
            .read_line(&mut convert_to_unit)
            .expect("Failed to read line");

        let convert_to_unit: String = match convert_to_unit.trim().parse() {
            Ok(unit) => unit,
            Err(_) => continue,
        };

        println!("You entered: {}", temp);
        println!("You entered: {}", convert_to_unit);

        let converted_temp: f64 = if convert_to_unit == "f" {
            convert_to_fahrenheit(temp)
        } else {
            convert_to_celcius(temp)
        };

        println!("{} is {}", temp, converted_temp);
        // break;
    }

    fn convert_to_celcius(f_temp: f64) -> f64 {
        (f_temp - 32.0) / 1.8
    }

    fn convert_to_fahrenheit(c_temp: f64) -> f64 {
        (c_temp * 1.8) + 32.0
    }
}
