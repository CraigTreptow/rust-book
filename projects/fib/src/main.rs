fn main() {
    for x in 0..40 {
        println!("Fib({}): {}", x, fibonacci(x));
    }
}

fn fibonacci(n: i32) -> i32 {
    match n {
        0 => return 0,
        1 => return 1,
        n => return fibonacci(n - 1) + fibonacci(n - 2),
    }
}
